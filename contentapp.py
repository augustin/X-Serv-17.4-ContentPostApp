
#!/usr/bin/python3

"""
 contentApp class
 Simple web application for managing content

 Copyright Jesus M. Gonzalez-Barahona, Gregorio Robles 2009-2015
 jgb, grex @ gsyc.es
 TSAI, SAT and SARO subjects (Universidad Rey Juan Carlos)
 October 2009 - March 2015
"""

import webapp


class contentApp (webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    content = {'/': 'Root page',
               '/page': 'A page'
               }

    def parse(self, request):
        """Return the resource name (including /)"""
        metodo = request.split(' ',2)[0]
        recurso = request.split(' ',2)[1]
        resto = request.split('\r\n\r\n', 1)[1]

        return (metodo, recurso, resto)

    def process(self, resourceName):
        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """

        metodo, recurso, resto = resourceName
        if metodo == "PUT" or metodo == "POST":
            self.content[recurso] = resto
            print(self.content)

        if recurso in self.content.keys():
            httpCode = "200 OK"
            htmlBody = "<html><body>" + self.content[recurso] \
                + "</body></html>"
        else:
            httpCode = "200 ok"
            htmlBody = """
            <html><body>
                <p>Si quiere aniadir una pagina nueva rellene el formulario</p>
                <form action="/Contenido" method="POST">
                    <br>
                    Contenido: <input name="content1" type="text" value="">
                    <br><br>
                    <input type="submit" value="Submit">
                </form>
            </body></html>
            """
        return (httpCode, htmlBody)

if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1234)